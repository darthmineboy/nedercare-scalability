This repository contains two samples for scalability:
* Docker Swarm
* Kubernetes

Build the Docker image from the "rest-api" folder:
```
mvn package dockerfile:build
```

# Docker Swarm
Deploy the stack. 
This will create a Docker container from the created "rest-api" image, and publish it to port 8080.
```
docker stack deploy --compose-file docker-compose.yml nedercare
```

Now execute a test script that sends 10 requests to the API. 
If you receive a blank output then the REST API has not finished starting; wait a few seconds and try again.
```
sh script.sh
```

You will receive:
```
You have been served by <SERVER-UUID> (x10)
```

The <SERVER-UUID> is expected to be the same in all lines.
This indicates the page is being served from the same REST API instance.

Now we can scale up the api service to 2 and execute the test script again. 
We now expect the <SERVER-UUID> to be different in some of the lines (2 variations).
If it is the same on all lines you should consider buying a lottery ticket.
```
docker service scale nedercare_restapi=2
sh script.sh
```

You can also visit the URL "localhost:8080/api/users" in the browser, 
however due to HTTP/2 keepalive the requests are not balanced as expected; you will be served by the same server.
You can use different browsers, or wait for the keepalive to expire, so that a new TCP connection is created.

Now to cleanup we can remove the stack.
```
docker stack rm nedercare
```

# Kubernetes
Import deployment & service.
```
kubectl apply -f restapi-deployment.yaml,restapi-service.yaml
```

Manually rescale. There is already a horizontal pod scaler configured, so this is optional.
```
kubectl scale --replicas=3 deployment/restapi
```

Info about horizontal pod autoscaler.
```
kubectl get hpa
kubectl describe hpa
```

With JMeter we can apply an artifical load. This project contains a 'test.jmx' file that can be run with JMeter.
```
jmeter -n -t test.jmx
```

To cleanup.
```
kubectl delete deployment restapi
kubectl delete service restapi
```