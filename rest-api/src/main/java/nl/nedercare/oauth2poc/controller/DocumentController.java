package nl.nedercare.oauth2poc.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class DocumentController {

    private static final UUID serverId = UUID.randomUUID();

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    String getAll() {
        long start = System.currentTimeMillis();
        while(System.currentTimeMillis() - start < 10) {
            DigestUtils.sha256Hex(System.currentTimeMillis() + "");
        }
        return "You have been served by " + serverId.toString();
    }

}
